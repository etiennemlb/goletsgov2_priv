// Copyright 2019 Etienne Malaboeuf. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package main implements client to server communication to achieve a data transmission.
// The data sent from the client to the server represent a workload.
//
// A client uses sockets. Top level function such as Dial uses the net.Conn
// package.
package main

import (
	"bufio"
	"fmt"
	"net"
	"sync"
	"os"
	//"time"
	"strconv"
	"strings"
)

func main() {

	var (
		ip   string
		port string
	)

	// We need an IP, PORT, LOOPCOUNT
	// LOOPCOUNT simulate a workload
	if len(os.Args) < 5 {
		fmt.Printf("Invalid arguments\n")
		os.Exit(1)
	}

	ip = os.Args[1]
	port = os.Args[2]	

	workLoad, err := strconv.Atoi(os.Args[3])
	if err != nil {
		fmt.Printf("The 3rd argument doesn't represent a number, parsing failure!\n")
		os.Exit(1)
	}

	requestCount, err := strconv.Atoi(os.Args[4])
	if err != nil {
		fmt.Printf("The 4rd argument doesn't represent a number, parsing failure!\n")
		os.Exit(1)
	}

	fmt.Print("Attempting to connect...\n")

	// attempting to connect to the server
	conn, err := net.Dial("tcp", ip+":"+port)
	if err != nil {
		panic(err)
	}

	var (
		bufferedSocket = bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
		request        string
		result         string
	)

	fmt.Print("Connexion successful sending workload...\n")

	request = fmt.Sprintf("SUBMIT WORKLOAD DATA=%d\n", workLoad)

/*
	// sending an unique request,
	// more can be sent using the same socket
	_, err = bufferedSocket.WriteString(request)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Sending request\n")
	bufferedSocket.Flush()
	// waiting for the server to answer
	result, err = bufferedSocket.ReadString('\n')
	result = strings.TrimSuffix(result, "\n")
	if len(result) > 5 && result[:4] == "DONE" {
		fmt.Printf("Workload ended correctly! (task: %s) \n", result[5:])
	} else {
		fmt.Printf("Workload processing failed! returned: %s : %s\n", result, err)
	}
*/	

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		for i := 0; i < requestCount; i++ {
			// sending an unique request
			// more can be sent using the same socket
			_, err = bufferedSocket.WriteString(request)
			if err != nil {
				panic(err)
			}
			fmt.Printf("Sending request\n")
			bufferedSocket.Flush()
			//time.Sleep(time.Duration(i) * time.Second)
		}
	}()

	go func() {		
		defer wg.Done()
		for i := 0; i < requestCount; i++ {
			// waiting for the server to answer
			result, err = bufferedSocket.ReadString('\n')
			result = strings.TrimSuffix(result, "\n")
			if len(result) > 5 && result[:4] == "DONE" {
				fmt.Printf("Workload ended correctly! (task: %s) \n", result[5:])
			} else {
				fmt.Printf("Workload processing failed! returned: %s : %s\n", result, err)
			}
		}
	}()

	wg.Wait()
}
