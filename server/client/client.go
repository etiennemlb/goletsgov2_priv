// Copyright 2019 Etienne Malaboeuf. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package client provides the Client struct.
// Client is composed of a socket, it could also be filled 
// with interesting data about the actual client to increase the 
// possibilites with loadbalancing and restriction with the number 
// of workload a client can submit
package client

import "net"

// CLient is a wrapper for a socket.
// It could be enriched with other clever stuff for loadbalancing
type Client struct {
	net.Conn
}
