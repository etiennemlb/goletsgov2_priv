// Copyright 2019 Etienne Malaboeuf. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package server provides the tools necessary to simulate a simple
// load processing server. It uses workers to process the workload. This 
// is achieved through a dispatcher(load balancing) and collector(gather the request from
// from the clients and formward it to the dispatcher.
//
// It uses "goletsgov2_priv/server/dispatcher" to dispatch the workload
// defined in "goletsgov2_priv/server/workers" to the workers
package server

import (
	"bufio"
	"fmt"
	"goletsgov2_priv/server/client"
	"goletsgov2_priv/server/dispatcher"
	"goletsgov2_priv/server/workers"
	"math/rand"
	"net"
	"time"
	"os"
	"sync"
	"strconv"
	"strings"
)

// Node represent an ip/port pair
type Node struct {
	_ip   string
	_port string
}

// Server represent a server
type Server struct {
	_node Node

	_idRNG *rand.Rand
	_nextNode Node

	//thread safe map go 1.9
	_idToClient sync.Map//map[uint64] client.Client
	
	_dispatcher    dispatcher.DispatcherInterface
	_fromCollector chan workers.WorkLoadInterface
}

// NewServer create a new, initialised server
func NewServer(ip string, port string, nextNodeIp string, nextNodePort string) *Server {
	// give the _fromCollector channel a buffer the keep the order of requested workload to process
	// indeed, mutex dont keep any ordering as when a goroutine asked for the data/value.
	// 128 should be enough for up to 128+WorkerNumber Workloed to be processed in requested order
	return &Server{
		_node: Node{_ip: ip, _port: port},
		_idRNG:         rand.New(rand.NewSource(time.Now().UTC().UnixNano())),
		_nextNode:      Node{_ip: nextNodeIp, _port: nextNodePort},
		//_idToClient: 	make(map[uint64] client.Client),
		_fromCollector: make(chan workers.WorkLoadInterface, 128),
	}
}

// isValidEndPoint allows to check if a node is correctly filled
// without meaningless values
func (n *Node) isValidEndPoint() bool {
	parts := strings.Split(n._ip, ".")

	if len(parts) < 4 {
		return false
	}

	for _, x := range parts {
		if i, err := strconv.Atoi(x); err == nil {
			if i < 0 || i > 255 {
				return false
			}
		} else {
			return false
		}
	}

	if p, err := strconv.Atoi(n._port); err == nil {
		if p < 1 || p > 65535 {
			return false
		}
	} else {
		return false
	}

	return true
}

// clientHandler handle a client and generate the correct workload for the associated commande recieved from
// the client (thus the ugly parsing done down below)
// clientObject is new client requesting something from the server
//
// soreuse means if true, that the socket is considered as waiting for an 
// answer after a request from the server it self to another server/entitie
// if false it means its waiting for an input (typicaly a request)
func (s *Server) collector(clientObject client.Client, soreuse bool) {

	var (
		clientIn = bufio.NewReader(clientObject)

		errno int
	)

	defer func() {
		if errno != 0 {
			clientObject.Close()

			switch errno {
			case 1:
				//fmt.Fprintf(os.Stderr, "Could not read socket, a Client or an other Server might have closed the socket\n")
			case 2:
				fmt.Fprintf(os.Stderr, "Wrong parameters in client request\n")
			case 3:
				fmt.Fprintf(os.Stderr, "Wrong parameters in client request: SUBMIT\n")
			case 4:
				fmt.Fprintf(os.Stderr, "Wrong parameters in client request: SUBMIT WORKLOAD DATA\n")
			case 5:
				fmt.Fprintf(os.Stderr, "Cannot receive request when waiting for an answer\n")
			case 6:
				fmt.Fprintf(os.Stderr, "Couldnt parse DATA to uint64: SUBMIT WORKLOAD DATA\n")
			case 7:
				fmt.Fprintf(os.Stderr, "Couldnt parse DATA to uint64: SUBMIT WORKLOAD DATA ID\n")
			case 8:
				fmt.Fprintf(os.Stderr, "Wrong parameters in client(or server) request: DONE\n")	
			case 9:
				fmt.Fprintf(os.Stderr, "Couldnt parse DATA to uint64: DONE ID\n")
			case 10:
				fmt.Fprintf(os.Stderr, "Not workers in current pool. Aborting...\n")
			case 11:
				fmt.Fprintf(os.Stderr, "Client not found in id map DONE ID\n")
			case 12:
				fmt.Fprintf(os.Stderr, "Wrong parameters in client(or server) request: DONE ID\n")			
			case 13:
				fmt.Fprintf(os.Stderr, "Could not forward request to an other server\n")
			case 14:
				fmt.Fprintf(os.Stderr, "Failed to connect to next node\n")
			default:
				return
			}
		}
	}()

	if !soreuse  {
		//fmt.Printf("New Connexion from %s \n", clientObject.RemoteAddr().String())
	} else {
		//fmt.Printf("Waiting on an answer from secondary server (%s)\n", clientObject.RemoteAddr().String())
	}

	for {
		line, err := clientIn.ReadString('\n')
		if err != nil {
			errno = 1
			return
		}

		//fmt.Printf("Got: %s", line)

		commands := strings.Split(line, " ")
		commandsLen := len(commands)

		if commandsLen < 1 {
			errno = 2
			return
		}

		switch commands[0] {
		case "SUBMIT":
			if soreuse {
				errno = 5
				return
			}
			if commandsLen < 3 {
				errno = 3
				return
			}
			switch commands[1] {
			case "WORKLOAD":

				switch commands[2][:4] {
				case "DATA":
					//    5  1  or more(\n)
					// DATA=XXX
					// DATA
					// XXX
					if len(commands[2]) < 5+1 {
						errno = 4					
						return
					}

					dataValue := strings.TrimSuffix(commands[2][5:], "\n")

					data, err := strconv.ParseUint(dataValue, 10, 64)
					if err != nil {
						errno = 6
						return
					}

					var (
						id uint64
						requestFromServer bool = true
					)

					//           3  1 or more
					// DATA=XXX ID=Y
					if commandsLen == 4 && commands[3][:3] == "ID=" && len(commands[3]) >= 3+1 {
						//fmt.Println("Has an ID, coming from server")
						idValue := strings.TrimSuffix(commands[3][3:], "\n")
						id, _ = strconv.ParseUint(idValue, 10, 64)
						if err != nil {
							errno = 7
							return
						}
					} else {
						requestFromServer = false
						id = uint64(s._idRNG.Uint64())
					}
			
	
					// if worker available
					// 		do work
					// else if HAS next node and ! isPresent
					//		store current socket with current ID
					//		forward to next node
					// else if no next node
					// 		wait for worker
					if  _, isPresent := s._idToClient.Load(id); 
					s._dispatcher.WorkersAvailable() == 0 && 
					s._nextNode.isValidEndPoint() && 
					!isPresent  { // is present protect against server loop ( A -> B -> A)
						fmt.Printf("Server has no available workers.\n")
						fmt.Printf("    Server has a valid next endpoint.\n")

						fmt.Printf("    Forwarding to next endpoint whit ID = %d\n", id)

						// store current socket with current ID
						// forward to next node

						//s._idToClient[id] = clientObject
						s._idToClient.Store(id, clientObject)

						// send the request to an other server
						serverToServer, err := net.Dial("tcp", s._nextNode._ip+":"+s._nextNode._port)
						if err != nil {
							fmt.Println(err)
							//errno = 14
							//return
						}

						serverToServerBuffered := bufio.NewWriter(serverToServer)

						var (
							newRequest string
						)

						if requestFromServer {
							newRequest = line
						} else {
							newRequest = strings.TrimSuffix(line, "\n")
							newRequest = fmt.Sprintf("%s ID=%d\n", newRequest, id)
						}

						_, err = serverToServerBuffered.WriteString(newRequest)
						if err != nil {
							errno = 13
							return
						}
					
						serverToServerBuffered.Flush()

						go s.collector(client.Client{serverToServer}, true)

					} else if s._dispatcher.WorkerCount() > 0 {
		
						s._fromCollector <- workers.NewWorkLoad(clientObject, id, data)
						fmt.Printf("New task added to queue: ID = %d\n", id)

					} else {
						errno = 10
						return
					}
				default:
					return
				}
			default:
				return
			}

		case "DONE":
			if commandsLen < 2 {
				errno = 8
				return
			}
			if commands[1][:3] != "ID=" {
				fmt.Println("error", commands[1])
				errno = 12
				return
			}

			value := strings.TrimSuffix(commands[1][3:], "\n")
			id, iderr := strconv.ParseUint(value, 10, 64)
			if iderr != nil {
				errno = 9
				return
			}

			// search the correct socket associated to the ID
			//		if the socket is present in the map send the done message aka the same we received
			//		delete the socket from map
			//		close the socket
			cli, ok := s._idToClient.Load(id)//s._idToClient[id]
			if ok == false {
				errno = 11
				return
			} 

			bufferedClient := bufio.NewWriter(cli.(client.Client))
			bufferedClient.WriteString(line)
			bufferedClient.Flush()

			// only the client can close the socket except if there is and ID,
			// in this case the client is a server and we must close the socket

			// a server only create a connection to an other server for
			// one workload. If there is an other workload it will create an other one,
			// we can close the socket connected to the secondary server without worry

			// close the connection to the secondary server (not to the client who requested the workload)
			clientObject.Close()

			s._idToClient.Delete(id)//delete(s._idToClient, id)
			return

		default:
			return
		}
	}
}

// Start starts the listening process
func (s *Server) Start() {

	/**
	 * We listen on the given port, bound the the given ip
	 */
	listener, listenErr := net.Listen("tcp", s._node._ip+":"+s._node._port)
	if listenErr != nil {
		panic(listenErr)
	}

	fmt.Printf("Listening on %s:%s\n", s._node._ip, s._node._port)

	for {
		/**
		 * Wait for a new connection to be established
		 */
		cliSocket, acceptErr := listener.Accept()
		if acceptErr != nil {
			panic(acceptErr)
		}

		go s.collector(client.Client{cliSocket}, false)
	}
}

// AttachDispatcher attach a dispatcher to the server for load distrubution
// dispatcher can thus be reimplemented to fit other needs (ei load balancing etc..)
func (s *Server) AttachDispatcher(dispatcher dispatcher.DispatcherInterface) {
	s._dispatcher = dispatcher
	go dispatcher.Dispatch(s._fromCollector)
}
