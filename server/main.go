// Copyright 2019 Etienne Malaboeuf. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package main implements client to server communication to achieve a data transmission.
// The data sent from the client to the server represent a workload.
//
// A client uses sockets. Top level function such as Dial uses the net.Conn
// package.
package main

import (
	"fmt"
	"goletsgov2_priv/server/dispatcher"
	"goletsgov2_priv/server/server"
	"os"
)

func main() {

	var (
		ip   string
		port string

		// when the server doesn't have any worker available it uses this backup ip:port to forward the workload
		nextNodeIp   string
		nextNodePort string
	)

	// server IP PORT NEXTNODE_IP NEXTNODE_PORT
	if len(os.Args) < 3 /*|| len(os.Args) > 3*/ {
		fmt.Printf("Invalid arguments\n")
		os.Exit(1)
	}

	// main server ip and port
	ip = os.Args[1]
	port = os.Args[2]

	 // when the server doesn't have any worker available it uses this backup ip:port to forward the workload
	if len(os.Args) == 5 {
		nextNodeIp = os.Args[3]
		nextNodePort = os.Args[4]
	}

	server := server.NewServer(ip, port, nextNodeIp, nextNodePort)

	// Modern processors might have ~ 8 cores
	dispatcher := dispatcher.NewDispatcher(1)

	// All workers are now waiting for a workload
	dispatcher.StartWorker()

	// The server needs a way to dispatch workloads on the worker pool
	server.AttachDispatcher(dispatcher)

	// The server is now listening
	server.Start()
}
