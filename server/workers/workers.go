// Copyright 2019 Etienne Malaboeuf. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package workers provides the tools necessary implement a 
// worker used to process a task.
//
// A WorkLoad must be constructed the New* function
package workers

import (
	"bufio"
	"fmt"
	"goletsgov2_priv/server/client"
	"sync/atomic"
	"time"
)

// WorkLoadInterface need a doWork function
// representing the workload
type WorkLoadInterface interface {
	doWork()
	cleanUp()
}

// WorkLoad represent a fake workload in the case
// it can be reimplemented to suit your needs
type WorkLoad struct {
	_client     client.Client
	_workLoadId uint64
	_workLoad   uint64
}

// NewWorkLoad create a WorkLoad
func NewWorkLoad(client client.Client, id uint64, workload uint64) *WorkLoad {
	return &WorkLoad{
		_client:     client,
		_workLoadId: id,
		_workLoad:   workload,
	}
}

// Worker is the struct representing a worker, it can be replaced by
// a simble function doing the same job.
type Worker struct {
	WorkerChan   chan WorkLoadInterface
	SetAvailable chan (chan WorkLoadInterface)

	isAvailable uint32
}

// doWork does is the main function of the
// WorkLoad
func (w *WorkLoad) doWork() {
	/*
		var (
			a int64 = 1
			b int64 = 0
		)
		//var ii = i
		//i+=1
		//fmt.Printf("Starting %d\n", ii)
		for ; w._workLoad != 0; w._workLoad-- {
			a, b = b, a
		}
		//fmt.Printf("Ending   %d\n", ii)
	*/
	time.Sleep(time.Duration(w._workLoad) * time.Second)
}

// cleanup is a function called when the worker has finished his task
func (w *WorkLoad) cleanUp() {

	var (
		clientOut     = bufio.NewWriter(w._client)
		replyToClient string
	)

	fmt.Printf("A task just finished! ID = %d\n", w._workLoadId)

	replyToClient = fmt.Sprintf("DONE ID=%d\n", w._workLoadId)

	clientOut.WriteString(replyToClient)
	clientOut.Flush()
}

// Start starts the worker
// the worker then wait for a load on its channel, when the work has been done
// and/or the worker is free, it subscribe its own load input channel
// to the dispatcher's channel
func (w *Worker) Start() {

	for {
		// prevent false result when computing the number of available workers
		w.setAvailability(true)
		w.SetAvailable <- w.WorkerChan

		select {
		case workload := <-w.WorkerChan:
			w.setAvailability(false)
			//workload.prepare()
			workload.doWork()
			workload.cleanUp()
		}
	}
}

// Set sets the availability of a worker
func (w *Worker) setAvailability(b bool) bool {

	if b {
		atomic.StoreUint32(&w.isAvailable, 1)
		return b
	}
	
	atomic.StoreUint32(&w.isAvailable, 0)
	return b
}

// Available return the availability of the worker
func (w *Worker) Available() bool {
	v := atomic.LoadUint32(&w.isAvailable)

	if v == 0 {
		return false
	}

	return true
}