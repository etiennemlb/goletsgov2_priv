// Copyright 2019 Etienne Malaboeuf. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package dispatcher provides the tools necessary to simulate a simple
// load balancing service. It uses workers to process the workload. 
// This implementation just associate a workload to the first worker available
// but could easily be improved
//
// It uses "goletsgov2_priv/server/workers"  as worker implementation
package dispatcher

import (
	"goletsgov2_priv/server/workers"
)

type DispatcherInterface interface {
	Dispatch(<-chan workers.WorkLoadInterface)
	WorkersAvailable() uint32
	WorkerCount() uint16
}

// Dispatcher dispatch the workloads on the available workers
type Dispatcher struct {
	_workerCount uint16
	_workers    []*workers.Worker

	_availableWorker chan (chan workers.WorkLoadInterface)
}

// NewDispatcher create a dipatcher
func NewDispatcher(workerCount uint16) *Dispatcher {
	return &Dispatcher{
		_workerCount: workerCount,
		_workers:    make([]*workers.Worker, workerCount),
		// buffered channel to keep the order in wich workers ask for a workload
		_availableWorker: make(chan (chan workers.WorkLoadInterface), workerCount)}
}

// StartWorker starts all workers
// if workers where to be function, this function would
// launch all the goroutine
func (d *Dispatcher) StartWorker() {
	var (
		i uint16
	)

	for ; i < d.WorkerCount(); i++ {
		// d._workers doesnt seem necessary, we could delete it
		// and instead of allocating a slice of worker, use a "lambda"
		// with, as parametters, the fields of the struct Worker

		// func(_availableWorker) {
		//	_workerChan := make(chan WorkLoadInterface)
		//	for {
		//		_availableWorker <- _workerChan
		//		select {
		//		case workload := <-w._workerChan:
		//			workload.doWork()
		//		}
		//	}
		// }
		d._workers[i] = &workers.Worker{
			// unbuffered channel
			// if the worker sent his input channel as available, it should be waiting on
			// an input from this channel thusn no buffering needed
			WorkerChan:   make(chan workers.WorkLoadInterface),
			SetAvailable: d._availableWorker,
		}

		go d._workers[i].Start()
	}
}

// Dispatch dispatches the load recieved by the server onto the workers
func (d *Dispatcher) Dispatch(fromCollector <-chan workers.WorkLoadInterface) {
	for {
		select {
		case workload := <-fromCollector:
			select {
			case availableWorker := <-d._availableWorker:
				availableWorker <- workload
			}
		}
	}
}

// WorkersAvailable compute the number of available worker
func (d *Dispatcher) WorkersAvailable() uint32 {
	var (
		count uint32
	)

	for i, _ := range d._workers {
		if d._workers[i].Available() {
			count++			
		}
	}
	return count
}

func (d *Dispatcher) WorkerCount() uint16 {
	return d._workerCount
}